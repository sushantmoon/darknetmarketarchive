import os


def get_all_files_path(root, output_file_name):
    f = open(output_file_name, 'a')
    for path, subdirs, files in os.walk(root):
        for name in files:
            f.write(str(os.path.join(path, name)) + '\n')
            # print(os.path.join(path, name))
    f.close()

if __name__ == "__main__":
    root = "F:\\DarknetMarket\\dnmarchives\\grams.tar\\"
    output_file_name = "file_paths_grams"
    get_all_files_path(root, output_file_name)