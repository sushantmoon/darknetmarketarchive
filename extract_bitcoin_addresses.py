import os
import re
import pandas as pd

def extract_bitcoin_address(file_paths, result_file):
    pattern = re.compile("^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$")
    with open(file_paths, 'r') as files:
        for individual_file in files:
            if individual_file:
                f = open(individual_file.strip(), 'r', errors = "ignore")
                data = [ x.strip() for x in f.read().split() ]
                matches = list(filter(pattern.match, data))
                if matches:
                    results = open(result_file, 'a')
                    for i in range(len(matches)):
                        results.write(str(individual_file.strip()) + ", " + str(i+1) + ", " + str(matches[i]) + "\n")
                    results.close()
                    # print(individual_file.strip())
                    # print(matches)
                    # break
    return 

def __pandas_subroutine_bitcoin_address_search__(individual_file, result_file):
    pattern = re.compile("^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$")
    if individual_file:
        f = open(individual_file.strip(), 'r', errors = "ignore")
        data = [ x.strip() for x in f.read().split() ]
        matches = list(filter(pattern.match, data))
        if matches:
            results = open(result_file, 'a')
            for i in range(len(matches)):
                results.write(str(individual_file.strip()) + ", " + str(i+1) + ", " + str(matches[i]) + "\n")
            results.close()
    return True

def extract_bitcoin_addresses_pandas(file_search_files_paths, result_file_pd):
    files = pd.read_csv(file_search_files_paths, header=None)
    result = files.applymap(lambda x: __pandas_subroutine_bitcoin_address_search__(x, result_file_pd))
    print(result.describe())

    

if __name__ == "__main__":
    file_with_paths = "file_paths_grams"
    result_file = "possible_address"
    result_file_pd = "possible_address_pandas"
    # extract_bitcoin_address(file_with_paths, result_file)
    extract_bitcoin_addresses_pandas(file_with_paths, result_file_pd)
