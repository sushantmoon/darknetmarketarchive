# Grep Commands 

```sh
    grep -r -E -i -w " 0x[a-z0-9]{40} " output
    grep -r -E -i -w "[[:space:]]0x[a-z0-9]{40}[[:space:]]" *
    grep -r -E -i -w "[[:space:]]0x[a-z0-9]{40}[[:space:]]" * > output &
    grep -r -E -i -w " 0x[a-z0-9]{40} " * > output &
    grep -r -E -i -w "\s0x[a-z0-9]{40}\s" * > output &
    grep -r -E -i -w "[^a-zA-Z\d]0x[a-z0-9]{40}[^a-zA-Z\d]" * > output &
    grep -r -E -i -w "(^0x)|([[:space:]]0x)[a-z0-9]{40}" testing-grep
```

## For bitcoin address

```sh
    grep -r -E -w "^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$" *
    grep -r -E -w "^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$" *
    grep -r -E -w "(^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$)|(^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$)" *
```


# Samples

```sh
0xb794F5eA0ba39494cE839613fffBA74279579268 0xb794F5eA0ba39494cE839613fffBA74279579268 
   0xb794F5eA0ba39494cE839613fffBA74279579268 
0xb794F5eA0ba39494cE839613fffBA742795792680xb794F5eA0ba39494cE839613fffBA74279579268 
0xb794F5eA0ba39494cE839613fffBA742795792 68
fxb794F5eA0ba39494cE839613fffBA74279579268
1xb794F5eA0ba39494cE839613fffBA74279579268
$% 0xb794F5eA0ba39494cE839613fffBA74279579268#0xb794F5eA0ba39494cE839613fffBA742795792680x b794F5eA0ba39494c E839613fffBA74279579268

1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2
3J98t1WpEZ73CNmQviecrnyiWrnqRhWNLy
bc1qar0srrr7xfkvy5l643lydnw9re59gtzzwf5mdq

BTC Address: 1JHwenDp9A98XdjfYkHKyiE3R99Q72K9X4 
BTC Address: 1Unoc4af6gCq3xzdDFmGLpq18jbTW1nZD
BTC Address: 1A8Ad7VbWDqwmRY6nSHtFcTqfW2XioXNmj
BTC Address: 12CZYvgNZ2ze3fGPFzgbSCELBJ6zzp2cWc
BTC Address: 17drmHLZMsCRWz48RchWfrz9Chx1osLe67

Receiving Bitcoin Address: 15LZALXitpbkK6m2QcbeQp6McqMvgeTnY8
Receiving Bitcoin Address: 1MAFzYQhm6msF2Dxo3Nbox7i61XvgQ7og5
```