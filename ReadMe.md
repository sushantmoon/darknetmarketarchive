# Extracting Bitcoin Addresses 

This aims at extracting/parsing the files in an folder to extract strings which are of the form of bitcoin addresses.

## Files

    1. listFiles.py 
        - generates a files (file name is defined `output_file_name`) containing the paths to different files contained the the directory pointed by `root` variable
        - run it using the command `python listFiles.py`
    
    2. extract_bitcoin_addresses.py
        - Takes in file `file_with_paths` with contains the paths to the list of the files to be processed.
        - Results are stores in "FileName, Serial Number, Possible Bitcoin Address" format to the files defined under : `result_file` or `result_file_pd` depending upon the method used.
        - function `extract_bitcoin_address` processes the file one by one and prints the results to the file defined under `result_file`
        - function `extract_bitcoin_addresses_pandas` uses pandas library to map the files paths to the function which processes the file at that path, extract address and outputs results to file defined under `result_file_pd`

    3. dnmarchives_archive.torrent 
        - torrent file to download the DarknetMarket Archive
    
    4. Regex-Testing.md
        - differnet regex commands for testing bitcoin address parsing, few samples are also given.